all:
	go build
	docker build --rm --tag goern-docker-registry.bintray.io/flight12/clie:develop .

push: all
	docker push goern-docker-registry.bintray.io/flight12/clie:develop

undeploy:
	oc delete job flight	

clean:
	rm clie
	docker rmi goern-docker-registry.bintray.io/flight12/clie:develop
