package model

import (
	"fmt"
	"time"
)

//FlightPlan is holding basic information about an ICAO Flight Plan
type Flightplan struct {
	ID                     string    `json:"-"`
	DepartureAerodrome     string    `json:"departure-aerodrome"`
	CruisingSpeed          int       `json:"cruising-speed"`
	DestinationAerodrome   string    `json:"destination-aerodrome"`
	DateOfFlight           time.Time `json:"date-of-flight"`
	AircraftIdentification string    `json:"aircraft-identification"`
	TotalEet               int       `json:"total-eet"`
	PersonsOnBoard         int       `json:"persons-on-board"`
	FlightStatus           string    `json:"flight-status"`
}

//GetID to satisfy jsonapi.MarshalIdentifier interface
func (f Flightplan) GetID() string {
	return f.ID
}

//SetID to satisfy jsonapi.UnmarshalIdentifier interface
func (f *Flightplan) SetID(stringID string) error {
	f.ID = stringID

	return nil
}

func (f Flightplan) String() string {
	return fmt.Sprintf("Flightplan %s to depart from %s to %s at %s, aircraft %s, %d persons on board and the total estimated enroute time is %d hours, flight status is '%s'",
		f.ID, f.DepartureAerodrome, f.DestinationAerodrome, f.DateOfFlight, f.AircraftIdentification, f.PersonsOnBoard, f.TotalEet, f.FlightStatus)
}
