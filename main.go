// Copyright © 2017 Christoph Görn <goern@b4mad.net>
//

package main

import (
	"os"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/flight12/clie/cmd"
)

// set at compile time
var (
	program string
	version string
)

func init() {
	// Log as JSON instead of the default ASCII formatter.
	// log.SetFormatter(&log.JSONFormatter{})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	log.SetLevel(log.InfoLevel)
}

func main() {
	cmd.Execute()
}
