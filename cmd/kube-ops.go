// Copyright © 2017 Christoph Görn goern@b4mad.net
//
// This file is part of clie.
//
// clie is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// clie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with clie. If not, see <http://www.gnu.org/licenses/>.
//

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	kubeConfig    string
	kubeServerURL string
	kubeToken     string
	kubeInside    bool
)

// kube-opsCmd represents the kube-ops command
var kubeOpsCmd = &cobra.Command{
	Use:   "kube-ops",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		// TODO: Work your own magic her
		fmt.Println("kubeOpsCmd called")
	},
}

func init() {
	RootCmd.AddCommand(kubeOpsCmd)

	kubeOpsCmd.PersistentFlags().StringVarP(&kubeServerURL, "kubernetes-server-url", "s", "http://localhost:8001/", "Kubernetes API URL")
	kubeOpsCmd.PersistentFlags().StringVarP(&kubeToken, "kubernetes-token", "t", "", "a Kubernetes API Token")
	kubeOpsCmd.PersistentFlags().StringVarP(&kubeConfig, "kubeconfig", "c", "", "absolute path to Kubernetes configuration file")
	kubeOpsCmd.PersistentFlags().BoolVarP(&kubeInside, "inside-job", "i", false, "assume we are running inside a Kubernetes cluster")

	viper.BindPFlag("kubernetes-server-url", kubeOpsCmd.PersistentFlags().Lookup("kubernetes-server-url"))
	viper.BindPFlag("kubernetes-token", kubeOpsCmd.PersistentFlags().Lookup("kubernetes-token"))
	viper.BindPFlag("kuberconfig", kubeOpsCmd.PersistentFlags().Lookup("kubeconfig"))
	viper.BindPFlag("inside-job", kubeOpsCmd.PersistentFlags().Lookup("indise-job"))

}
