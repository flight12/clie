// Copyright © 2017 Christoph Görn goern@b4mad.net
//
// This file is part of clie.
//
// clie is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// clie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with clie. If not, see <http://www.gnu.org/licenses/>.
//

package cmd

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/manyminds/api2go/jsonapi"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	flight12 "gitlab.com/flight12/clie/model"
)

var outputAs string

// getCmd respresents the get command
var getCmd = &cobra.Command{
	Use:   "get ID",
	Short: "Get a Flightplan",
	Long:  `This command will get a specific Flightplan, you need to provide the Flightplan ID`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			log.Fatal("need exactly one FlightplanID")
		}
		fpid := args[0]

		log.Debugf("in getCmd: service-url=%s, ID=%s", serviceURL, fpid)

		client := &http.Client{Timeout: 4 * time.Second}

		req, _ := http.NewRequest("GET", FlightplanServiceURL+"/"+fpid, nil)
		req.Header.Add("Accept", "application/vnd.api+json") // without and with mediatype is required
		req.Header.Add("Accept", "application/vnd.api+json; version=0.1.0-alpha1")

		response, err := client.Do(req)

		if err != nil {
			log.Fatal(err)
		} else {
			defer response.Body.Close()

			log.Debugf("in getCmd: %v", response.Status)
			if response.StatusCode == 200 {
				body, err := ioutil.ReadAll(response.Body)

				var fp flight12.Flightplan
				err = jsonapi.Unmarshal(body, &fp)
				if err != nil {
					log.Fatal(err)
				}

				// show what we got
				log.Debugf("%v", fp)
				fmt.Printf("%s\n", fp) // TODO respect outputAs
			} else if response.StatusCode == 404 {
				fmt.Printf("Flightplan with ID %s cant be found!\n", fpid) // TODO respect outputAs
			}
		}
	},
}

func init() {
	flightplanCmd.AddCommand(getCmd)

	getCmd.PersistentFlags().StringVarP(&outputAs, "output", "o", "yaml", "Output format. One of: {json|yaml}")
	viper.BindPFlag("outputAs", getCmd.PersistentFlags().Lookup("output"))

}
