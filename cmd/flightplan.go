// Copyright © 2017 Christoph Görn goern@b4mad.net
//
// This file is part of clie.
//
// clie is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// clie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with clie. If not, see <http://www.gnu.org/licenses/>.
//

package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var FlightplanServiceURL string

// flightplanCmd respresents the flightplan command
var flightplanCmd = &cobra.Command{
	Use:   "flightplan",
	Short: "manage Flightplans",
	Long:  `This command is used to create, list, modify Flightplans.`,
}

func init() {
	RootCmd.AddCommand(flightplanCmd)

	flightplanCmd.PersistentFlags().StringVarP(&FlightplanServiceURL, "flightplan-service-url", "s", "http://localhost:3000/flightplans", "Flightplan service URL")
	viper.BindPFlag("flightplan-service-url", flightplanCmd.PersistentFlags().Lookup("flightplan-service-url"))

}
