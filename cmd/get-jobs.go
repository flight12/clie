// Copyright © 2017 Christoph Görn goern@b4mad.net
//
// This file is part of clie.
//
// clie is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// clie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with clie. If not, see <http://www.gnu.org/licenses/>.
//

package cmd

import (
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"

	log "github.com/Sirupsen/logrus"

	"github.com/spf13/cobra"
)

var (
	config    *rest.Config
	clientset *kubernetes.Clientset
)

// get-jobsCmd represents the get-jobs command
var getJobsCmd = &cobra.Command{
	Use:   "get-jobs",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("getJobsCmd called")

		if kubeInside {
			config, _ = rest.InClusterConfig()

		} else {
			config, _ = clientcmd.BuildConfigFromFlags("", kubeConfig)

		}

		log.Debugf("Kubernetes Configuration: %v", config)
		if config == nil {
			log.Fatal("can not find any kubernetes config")
			return
		}

		// creates the clientset
		clientset, err := kubernetes.NewForConfig(config)
		if err != nil {
			log.Panic(err.Error())
		}

		log.Debugf("Kubernetes Client: %v", clientset)

		info, err := clientset.Discovery().ServerVersion()
		if err != nil {
			log.Fatalf("failed to retrieve server API version: %s", err)
		}

		log.Debugf("server API version information: %s", info)

		pods, err := clientset.BatchV1().Jobs("myproject").List(metav1.ListOptions{})
		if err != nil {
			log.Fatal(err.Error())
		}

		fmt.Printf("There are %d Jobs in the cluster", len(pods.Items))
		for index, pod := range pods.Items {
			fmt.Printf("%d - %v", index, pod)
		}
	},
}

func init() {
	kubeOpsCmd.AddCommand(getJobsCmd)
}
