// Copyright © 2017 Christoph Görn goern@b4mad.net
//
// This file is part of clie.
//
// clie is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// clie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with clie. If not, see <http://www.gnu.org/licenses/>.
//

package cmd

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	log "github.com/Sirupsen/logrus"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

//FlightplanVersion is used to keep the Flightplan API latest version
type FlightplanVersion struct {
	Service     string `json:"service"`
	Version     string `json:"version"`
	Description string `json:"description"`
}

//Version keeps all version information gatheres
type Version struct {
	Client        string             `json:"client"`
	FlightplanAPI *FlightplanVersion `json:"flightplan-api"`
}

func getFlightplanAPIVersion(url string, target interface{}) error {
	client := &http.Client{Timeout: 4 * time.Second}

	req, _ := http.NewRequest("HEAD", FlightplanServiceURL, nil)
	req.Header.Add("Accept", "application/json")

	r, err := client.Do(req)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

var serviceURL string

// versionCmd respresents the version command
var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print versions of CLI and API",
	Long:  `Provide information about the CLI version, all API used and the server software versions in use.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("clie CLI: v0.1.0-alpha1\n\n")

		versions := new(Version)
		flightplanAPIVersion := new(FlightplanVersion)
		versions.FlightplanAPI = flightplanAPIVersion

		err := getFlightplanAPIVersion(serviceURL, flightplanAPIVersion)
		if err != nil {
			log.Debug("%v\n", err)
			fmt.Println("Flightplan API: <unavailable>")
			return
		}

		fmt.Printf("Flightplan API: %s\n", flightplanAPIVersion.Version)
	},
}

func init() {
	RootCmd.AddCommand(versionCmd)

	versionCmd.PersistentFlags().StringVarP(&serviceURL, "service-url", "", "http://localhost:3000", "Flightplan service URL")
	viper.BindPFlag("service-url", versionCmd.PersistentFlags().Lookup("service-url"))
}
