// Copyright © 2017 Christoph Görn goern@b4mad.net
//
// This file is part of clie.
//
// clie is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// clie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with clie. If not, see <http://www.gnu.org/licenses/>.
//

package cmd

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/manyminds/api2go/jsonapi"
	"github.com/spf13/cobra"

	log "github.com/Sirupsen/logrus"

	flight12 "gitlab.com/flight12/clie/model"
)

// listCmd respresents the list command
var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List all existing Flightplans",
	Long:  `List all existing Flightplans`,
	Run: func(cmd *cobra.Command, args []string) {
		// TODO: Work your own magic here
		log.Debugf("in listCmd: service-url=%s", serviceURL)

		client := &http.Client{Timeout: 4 * time.Second}

		req, _ := http.NewRequest("GET", FlightplanServiceURL, nil)
		req.Header.Add("Accept", "application/vnd.api+json") // without and with mediatype is required
		req.Header.Add("Accept", "application/vnd.api+json; version=0.1.0-alpha1")

		log.Debugf("%v\n", req)

		response, err := client.Do(req)

		if err != nil {
			log.Fatal(err)
		} else {
			defer response.Body.Close()

			log.Debugf("in listCmd: %v", response.Status)
			if response.StatusCode == 200 {
				body, err := ioutil.ReadAll(response.Body)

				var fp []flight12.Flightplan
				err = jsonapi.Unmarshal(body, &fp)
				if err != nil {
					log.Fatal(err)
				}

				// show what we got
				log.Debugf("%v", fp)
				for _, f := range fp {
					fmt.Printf("%s\n", f)
				}
			} // TODO catch a 404
		}
	},
}

func init() {
	flightplanCmd.AddCommand(listCmd)

}
