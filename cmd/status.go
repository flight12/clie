// Copyright © 2017 Christoph Görn <goern@b4mad.net>
//

package cmd

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	log "github.com/Sirupsen/logrus"

	"github.com/manyminds/api2go/jsonapi"
	"github.com/spf13/cobra"

	flight12 "gitlab.com/flight12/clie/model"
)

// statusCmd respresents the status command
var statusCmd = &cobra.Command{
	Use:   "status",
	Short: "This will give a Flightplan status",
	Long:  `This command will get a specific Flightplan status, you need to provide the Flightplan ID`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			log.Fatal("need exactly one FlightplanID")
		}
		fpid := args[0]

		log.Debugf("in statusCmd: service-url=%s, ID=%s", serviceURL, fpid)

		client := &http.Client{Timeout: 4 * time.Second}

		req, _ := http.NewRequest("GET", FlightplanServiceURL+"/"+fpid, nil)
		req.Header.Add("Accept", "application/vnd.api+json") // without and with mediatype is required
		req.Header.Add("Accept", "application/vnd.api+json; version=0.1.0-alpha1")

		response, err := client.Do(req)

		if err != nil {
			log.Fatal(err)
		} else {
			defer response.Body.Close()

			log.Debugf("in statusCmd: %v", response.Status)
			if response.StatusCode == 200 {
				body, err := ioutil.ReadAll(response.Body)

				var fp flight12.Flightplan
				err = jsonapi.Unmarshal(body, &fp)
				if err != nil {
					log.Fatal(err)
				}

				// show what we got
				log.Debugf("%v", fp)
				fmt.Printf("Flightplan %s is in '%s'.\n", fp.ID, fp.FlightStatus) // TODO respect outputAs
			} else if response.StatusCode == 404 {
				fmt.Printf("Flightplan with ID %s cant be found!\n", fpid) // TODO respect outputAs
			}
		}
	},
}

func init() {
	operationsCmd.AddCommand(statusCmd)

}
