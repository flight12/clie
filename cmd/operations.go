// Copyright © 2017 Christoph Görn goern@b4mad.net
//
// This file is part of clie.
//
// clie is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// clie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with clie. If not, see <http://www.gnu.org/licenses/>.
//

package cmd

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var flightplanID string

// TODO validArgs []string = { "taxi", "takeoff", "land" }

// operationsCmd respresents the operations command
var operationsCmd = &cobra.Command{
	Use:   "operations",
	Short: "This command is used for flight operations",
	Long:  `This command is use for all flight operations like takeoff and landing. A Flightplan ID is required.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("a subcommand is required")
		}
		fmt.Println("operations called")

		return nil
	},
	// TODO ValidArgs: validArgs,
}

func init() {
	RootCmd.AddCommand(operationsCmd)

	operationsCmd.PersistentFlags().StringVarP(&flightplanID, "flightplan-id", "f", "", "Flightplan ID (required")
	viper.BindPFlag("flightplan-id", operationsCmd.PersistentFlags().Lookup("flightplan-id"))

	operationsCmd.MarkFlagRequired("flightplan-id")
}
