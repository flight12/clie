// Copyright © 2017 Christoph Görn goern@b4mad.net
//
// This file is part of clie.
//
// clie is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// clie is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with clie. If not, see <http://www.gnu.org/licenses/>.
//

package cmd

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	log "github.com/Sirupsen/logrus"
)

var cfgFile string
var debugFlag bool

//RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "clie",
	Short: "The command line interface to all Flight12 services",
	Long:  `This is the command line interfact to all services provided by Flight12.`,
}

//Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings

	// Cobra supports Persistent Flags which if defined here will be global for your application
	RootCmd.PersistentFlags().BoolVarP(&debugFlag, "debug", "d", true, "additional debug output")
	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.clie.yaml)")

	viper.BindPFlag("debug", RootCmd.PersistentFlags().Lookup("debug"))

}

// Read in config file and ENV variables if set.
func initConfig() {
	// set default for debug
	viper.SetDefault("debug", true)

	if cfgFile != "" { // enable ability to specify config file via flag
		viper.SetConfigFile(cfgFile)
	}

	viper.SetConfigName(".flight12") // name of config file (without extension)
	viper.AddConfigPath("$HOME")     // adding home directory as first search path
	viper.AutomaticEnv()             // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}

	// from the environment
	viper.SetEnvPrefix("FLIGHT12")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	if viper.GetBool("debug") {
		log.SetLevel(log.DebugLevel)
	}

}
