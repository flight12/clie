package utils

// DecorateRuntimeContext appends line, file and function context to the logger
import (
	"runtime"

	"github.com/Sirupsen/logrus"
)

//DecorateRuntimeContext is used to decorate any logoutput
func DecorateRuntimeContext(logger *logrus.Entry) *logrus.Entry {
	if pc, file, line, ok := runtime.Caller(1); ok {
		fName := runtime.FuncForPC(pc).Name()
		return logger.WithField("file", file).WithField("line", line).WithField("func", fName)
	}
	return logger
}
