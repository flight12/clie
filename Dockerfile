FROM registry.access.redhat.com/rhel7-atomic
MAINTAINER Christoph Görn <goern@b4mad.net>

COPY clie /usr/bin

ENTRYPOINT ["/usr/bin/clie"]
CMD ["kube-ops", "--debug", "--inside-job", "get-jobs"]
